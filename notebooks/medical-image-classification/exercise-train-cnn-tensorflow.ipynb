{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Convolutional Neural Network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Modules](#Python-Modules)\n",
    "  * [Data](#Data)\n",
    "* [Teaching Content](#Teaching-Content)\n",
    "  * [Accessing our Preprocessed Data](#Accessing-our-Preprocessed-Data)\n",
    "* [Exercises](#Exercises)\n",
    "  * [Normalization](#Normalization)\n",
    "  * [Defining the Convolution Neural Network](#Defining-the-Convolution-Neural-Network)\n",
    "  * [Training](#Training)\n",
    "  * [Saving the Trained Model](#Saving-the-Trained-Model)\n",
    "  * [Plots](#Plots)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook you will define a Convolutional Neural Network (CNN), using the `tensorflow.keras` API, to predict, whether a tile contains tumorous tissue or not. Since the data handling process with a `generator` can be tricky you will be supplied with a class, which handles access to our HDF5 file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "### Python-Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TensorFlow and tf.keras\n",
    "import tensorflow as tf\n",
    "from tensorflow import keras\n",
    "\n",
    "# Helper libraries\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import random\n",
    "import h5py\n",
    "import math\n",
    "from datetime import datetime\n",
    "\n",
    "# Furcifar Modules\n",
    "from preprocessing.util import find_files\n",
    "from preprocessing.datamodel import SlideManager\n",
    "from preprocessing.util import TileMap\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "In this notebook we use perviously preprocessed data of the CAMELYON16 data set.\n",
    "\n",
    "To set the data set location please adjust the path bellow to the directory of preprocessed hdf5 files.\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "If you have the possibility, copy the generated HDF5 file onto SSD"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# EDIT THIS CELL (Option B of last notebook)\n",
    "# Path where your HDF5 file is located\n",
    "GENERATED_DATA = '/home/klaus/Documents/datasets/PN-STAGE/level3/'\n",
    "HDF5_FILE = GENERATED_DATA + 'all_wsis_256x256_poi0.2_poiTumor0.6_level3.hdf5'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# EDIT THIS CELL (Option A of last notebook)\n",
    "# Path where your HDF5 file is located\n",
    "#GENERATED_DATA = '/home/klaus/Documents/datasets/PN-STAGE/level0/'\n",
    "#HDF5_FILE = GENERATED_DATA + 'all_wsis_312x312_poi0.2_poiTumor0.6_level0.hdf5'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is where we save checkpoints for our model\n",
    "MODEL_CHECKPOINT = GENERATED_DATA + 'model_checkpoint.ckpt'\n",
    "MODEL_FINAL = GENERATED_DATA + 'model_final.hdf5'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let us see if we can access our HDF5 file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_file = h5py.File(HDF5_FILE,'r',libver='latest',swmr=True)\n",
    "print('List of data entires')\n",
    "for key in data_file.keys():\n",
    "    print(key, 'with shape', data_file[key].shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Teaching Content\n",
    "### Accessing our Preprocessed Data\n",
    "\n",
    "First we define a class which handles access to our generated HDF5 file. The requirements for this class are:\n",
    "- Return a batch of examples as 4D-numpy array (n_examples, tile-width, tile-height, color-channels) together with the labels for each example\n",
    "- Return specified number of examples of each class per batch\n",
    "- Randomly chose the examples of each class\n",
    "- Clearly split training and validation data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class TissueDataset():\n",
    "    \"\"\"Data set for preprocessed WSIs of the CAMELYON16 and CAMELYON17 data set.\"\"\"\n",
    "        \n",
    "    def __init__(self, path, percentage=.5, first_part=True):      \n",
    "        self.h5_file = path\n",
    "        self.h5 = h5py.File(path, 'r', libver='latest', swmr=True)\n",
    "        self.perc = percentage\n",
    "        self.first_part = first_part\n",
    "        self.dataset_names = list(self.h5.keys())\n",
    "        self.neg = [i for i in self.dataset_names if 'ormal' in i]\n",
    "        self.pos = [i for i in self.dataset_names if 'umor' in i]\n",
    "        self.dims = self.h5[self.neg[0]][0].shape\n",
    "    \n",
    "    def __get_tiles_from_path(self, dataset_names, max_wsis, number_tiles):\n",
    "        tiles = np.ndarray((number_tiles, 256, 256, 3))\n",
    "        for i in range(number_tiles):\n",
    "            file_idx = np.random.randint(0, max_wsis)\n",
    "            dset = self.h5[dataset_names[file_idx]]\n",
    "            len_ds = len(dset)\n",
    "            max_tiles = math.ceil(len_ds * self.perc)\n",
    "            if self.first_part:\n",
    "                rnd_idx = np.random.randint(0, max_tiles)\n",
    "            else:\n",
    "                rnd_idx = np.random.randint(len_ds - max_tiles, len_ds)\n",
    "            ### crop random 256x256\n",
    "            if self.dims[1] > 256:\n",
    "                rand_height = np.random.randint(0, self.dims[0]-256)\n",
    "                rand_width = np.random.randint(0, self.dims[1]-256)\n",
    "            else:\n",
    "                rand_height = 0\n",
    "                rand_width = 0\n",
    "            tiles[i] = dset[rnd_idx,rand_height:rand_height+256,rand_width:rand_width+256]\n",
    "        tiles = tiles / 255.\n",
    "        return tiles\n",
    "    \n",
    "    def __get_random_positive_tiles(self, number_tiles):\n",
    "        return self.__get_tiles_from_path(self.pos, len(self.pos), number_tiles), np.ones((number_tiles))\n",
    "    \n",
    "    def __get_random_negative_tiles(self, number_tiles):\n",
    "        return self.__get_tiles_from_path(self.neg, len(self.neg), number_tiles), np.zeros((number_tiles))\n",
    "    \n",
    "    def generator(self, num_neg=10, num_pos=10, data_augm=False, mean=[0.,0.,0.], std=[1.,1.,1.]):\n",
    "        while True:\n",
    "            x, y = self.get_batch(num_neg, num_pos, data_augm)\n",
    "            for i in [0,1,2]:\n",
    "                x[:,:,:,i] = (x[:,:,:,i] - mean[i]) / std[i]\n",
    "            yield x, y\n",
    "\n",
    "    def get_batch(self, num_neg=10, num_pos=10, data_augm=False):\n",
    "        x_p, y_p = self.__get_random_positive_tiles(num_pos)\n",
    "        x_n, y_n = self.__get_random_negative_tiles(num_neg)\n",
    "        x = np.concatenate((x_p, x_n), axis=0)\n",
    "        y = np.concatenate((y_p, y_n), axis=0)\n",
    "        if data_augm:\n",
    "            ### some data augmentation mirroring / rotation\n",
    "            if np.random.randint(0,2): x = np.flip(x, axis=1)\n",
    "            if np.random.randint(0,2): x = np.flip(x, axis=2)\n",
    "            x = np.rot90(m=x, k=np.random.randint(0,4), axes=(1,2))\n",
    "        ### randomly arrange in order\n",
    "        p = np.random.permutation(len(y))\n",
    "        return x[p], y[p]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now load our data set split into training and validation data.\n",
    "By setting `train_data = TissueDataset(path=HDF5_FILE,  percentage=0.5, first_part=True)`, we say that `training_data` consists of the first 50% of tiles of every WSI.\n",
    "\n",
    "The method `get_batch` returns a specified number of positive and negative slides. The slides are randomly shuffeled, so the first part of the batch does not only contain positive slides, and the last part of the batch only negtive slides."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "train_data = TissueDataset(path=HDF5_FILE,  percentage=0.5, first_part=True)\n",
    "val_data = TissueDataset(path=HDF5_FILE, percentage=0.5, first_part=False)\n",
    "\n",
    "x, y = train_data.get_batch(num_neg=3, num_pos=3)\n",
    "print(x.shape)\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `generator` method does the same as `get_batch`, but implements a python generator, which is very useful when training and evaluating a `tensorflow.keras` model.\n",
    "\n",
    "The method argument `data_augm=True` randomly rotates the batch zero to three times by 90 degrees and randomly flips is horizonally and / or vertically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By plotting the examples of a batch we can see, that the slides have been successfully stored in the HDF5 file and we are also able to successfully load them together with the labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12,4))\n",
    "\n",
    "itera = train_data.generator(num_neg=1, num_pos=1, data_augm=True)\n",
    "for x, y in itera:\n",
    "    print(x.shape)\n",
    "    for i in range(2):\n",
    "        ax = plt.subplot(1, 2, i + 1)\n",
    "        plt.tight_layout()\n",
    "        ax.set_title('Sample #{} - class {}'.format(i, y[i]))\n",
    "        ax.imshow(x[i])\n",
    "        ax.axis('off') \n",
    "    break # generate yields infinite random samples, so we stop after first"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "### Normalization\n",
    "\n",
    "There are far superior normalization methods for WSI-tasks. If you are interested you can read more about such methods in [MAG09] and [ROY18])\n",
    "\n",
    "For now, we will stick with a standard way of normalization by subtracting the mean and dividing by the standard deviation.\n",
    "\n",
    "So next we will fetch a big batch and calculate the mean and standard deviation **colorwise**.\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Query a big batch from the whole dataset (not just the test set). Make it as big as your RAM can handle, though 200 negative and 200 positive slides should be sufficient. Then calculate the mean and standard deviation for each color channel and save them into `mean_pixel` and `std_pixel`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_pixel = np.full((3),0.0)\n",
    "std_pixel = np.full((3),1.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Exercise: Query a big batch and save the mean and standard deviation \n",
    "### for each channel into the variable devined above"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also visualize the mean image and the image for the standard deviation. Although we do not need a full sized tile of them. The 1D-array is enough for normalization and also probably faster computation wise.\n",
    "\n",
    "But still visualizing them as full sized tiles gives some insight about the mean of the colors and their veriance.\n",
    "\n",
    "If everything is correct, executing the following cell will show the following image:\n",
    "\n",
    "![internet connection needed](https://gitlab.com/deep.TEACHING/educational-materials/raw/master/media/klaus/medical-image-classification/mean_and_std_dev_images.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_img = np.full((256,256,3),0.0)\n",
    "std_img = np.full((256,256,3),0.0)\n",
    "mean_img[:,:] = mean_pixel\n",
    "std_img[:,:] = std_pixel\n",
    "\n",
    "plt.figure(figsize=(12,4))\n",
    "\n",
    "# Mean image\n",
    "ax = plt.subplot(1, 2, 1)\n",
    "plt.tight_layout()\n",
    "ax.set_title('mean image')\n",
    "ax.imshow(mean_img)\n",
    "ax.axis('off')\n",
    "# Std Deviation image\n",
    "ax = plt.subplot(1, 2, 2)\n",
    "plt.tight_layout()\n",
    "ax.set_title('std deviation image')\n",
    "ax.imshow(std_img)\n",
    "ax.axis('off') \n",
    "\n",
    "print('Mean colors: ', mean_pixel)\n",
    "print('Std Dev colors: ', std_pixel)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By passing `mean_pixel` and `standard pixel` into the `generator` method we get normalized images. The images will look like something is wrong with them, but this is just because we now have pixel values centered around 0.0 (probably most likely from -3 to +3). `matplotlib` either wants integer from 0 to 255 or floats form 0.0 to 1.0. The CNN however will converge a lot faster and probably also a lot better in the end.\n",
    "\n",
    "Executing the cell below should yield an image similar to the following:\n",
    "\n",
    "![internet connection needed](https://gitlab.com/deep.TEACHING/educational-materials/raw/master/media/klaus/medical-image-classification/normalized_images.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12,4))\n",
    "\n",
    "itera = train_data.generator(1, 1, True, mean_pixel, std_pixel)\n",
    "for x, y in itera:\n",
    "    print(x.shape)\n",
    "    for i in range(2):\n",
    "        ax = plt.subplot(1, 2, i + 1)\n",
    "        plt.tight_layout()\n",
    "        ax.set_title('Sample #{} - class {}'.format(i, y[i]))\n",
    "        ax.imshow(x[i])\n",
    "        ax.axis('off') \n",
    "    break # generate yields infinite random samples, so we stop after first"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Defining the Convolution Neural Network\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Define a model for the training. Use classes / methods from `tensorflow.keras`. Also compile your model with an optimizer, a loss and a metric (e.g. _accuracy_). As we only have two classes, we can use the _binary crossentropy_ as loss function.\n",
    "\n",
    "#### Suggested Models\n",
    "\n",
    "If you do not feel like experimenting (takes a lot of time), we can suggest to try out the following model:\n",
    "    * InceptionResNetV2 with pretrained weights on ImageNet.\n",
    "    * We ditch pretrained last layers.\n",
    "    * Benefit is that CNNs tend to develop the same filters for the first convolution layers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "base_model = keras.applications.InceptionResNetV2(\n",
    "                                 include_top=False, \n",
    "                                 weights='imagenet', \n",
    "                                 input_shape=(256,256,3), \n",
    "                                 )\n",
    "\n",
    "x = base_model.output\n",
    "x = keras.layers.GlobalAveragePooling2D()(x)\n",
    "x = keras.layers.Dense(1024, activation='relu')(x)\n",
    "predictions = keras.layers.Dense(1, activation='sigmoid')(x)\n",
    "\n",
    "model = keras.Model(inputs=base_model.input, outputs=predictions)\n",
    "\n",
    "model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=0.0001), \n",
    "              loss='binary_crossentropy',\n",
    "              metrics=['accuracy'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# If you interrupted your training, load the checkpoint here:\n",
    "\n",
    "model.load_weights(MODEL_CHECKPOINT)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training\n",
    "\n",
    "**Task:**\n",
    "\n",
    "- Train and Evaluate your model.\n",
    "- Keep track of the metrics _loss_ and _accuracy_ for training and validation data, since we want to plot these later.\n",
    "\n",
    "**Hints:**\n",
    "\n",
    "- Make checkpoints every epoch.\n",
    "- Use the `fit_generator` method of your model and pass our `generator` method as parameter\n",
    "- The following parameters showed to work well using a **Geforce 1080 TI (11 GB RAM)**:\n",
    "    - Each epoch consists of 100 training batches and 50 validations batches\n",
    "    - Each batch contains 20 negatives and 20 positives\n",
    "    - Train for at least 50 epochs (15 hours on Geforce 1080 TI, slide-magnification level 3 (Option A) used)\n",
    "- If you want to try these settings but get an out of memory (OOM) error, adjust the batch size.\n",
    "\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "If you have the possibility, copy the generated HDF5 file onto SSD"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "train_accs = []\n",
    "train_losses = []\n",
    "val_accs = []\n",
    "val_losses = []\n",
    "\n",
    "batch_size_neg=20\n",
    "batch_size_pos=20\n",
    "batches_per_train_epoch = 100\n",
    "batches_per_val_epoch = 50\n",
    "epochs = 50"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create checkpoint callback\n",
    "cp_callback = tf.keras.callbacks.ModelCheckpoint(MODEL_CHECKPOINT, \n",
    "                                                 save_weights_only=True,\n",
    "                                                 save_best_only=True,\n",
    "                                                 verbose=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "now1 = datetime.now()\n",
    "\n",
    "### Uncomment to easily disable color normalization\n",
    "#mean_pixel = [0.,0.,0.] \n",
    "#std_pixel = [1.,1.,1.]  \n",
    "\n",
    "for i in range(epochs):\n",
    "    hist = model.fit_generator(\n",
    "            generator=train_data.generator(batch_size_neg, batch_size_pos, True, mean_pixel, std_pixel),\n",
    "            steps_per_epoch=batches_per_train_epoch, \n",
    "            validation_data=val_data.generator(batch_size_neg, batch_size_pos, False, mean_pixel, std_pixel),\n",
    "            validation_steps=batches_per_val_epoch,\n",
    "            callbacks=[cp_callback], workers=1, use_multiprocessing=False, max_queue_size=10)\n",
    "    \n",
    "    train_accs.append(hist.history['acc'])\n",
    "    train_losses.append(hist.history['loss'])\n",
    "    val_accs.append(hist.history['val_acc'])\n",
    "    val_losses.append(hist.history['val_loss'])\n",
    "\n",
    "now2 = datetime.now()\n",
    "print(now2 - now1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving the Trained Model\n",
    "\n",
    "Since checkpoints only save weights, now save the whole model, so we won't need the code to generate the model in the next notebooks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save entire model to a HDF5 file\n",
    "model.save(MODEL_FINAL)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plots\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Plot the progress of your training over the epochs. That means the _loss_ and the _accuracy_ for the validation as well as the training data\n",
    "\n",
    "Your plots should look similar to these if training was successful:\n",
    "\n",
    "<img src=\"https://gitlab.com/deep.TEACHING/educational-materials/raw/master/media/klaus/medical-image-classification/cnn_resultsplot_vgg_no_dense_120_epoch.png\" width=\"768\" alt=\"internet connection needed\">\n",
    "\n",
    "**Note:**\n",
    "\n",
    "The reason that the curves are not smooth is because we do no always feed the whole dataset, neither for training, nor for validation. So it happens, that some batches contain tiles, which are easier to predict, or extremely hard to predict. So therefore the spikes.\n",
    "\n",
    "The image above was created with a CNN with no pretrained weights. So if you have used the code provided with pretrained weights on ImageNet, ~90% accuracy will already be reached after the first couple of epochs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Exercise: Plot the training progress"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "After this notebook you have a trained CNN, which is able to predict whether a slide contain tumorous tissue or not.\n",
    "\n",
    "In the next notebook you will use this model to predict the tiles of the slides of the CAMELYON16 test set and to produce heatmaps of the slides.\n",
    "\n",
    "Possibilities to improve your CNN:\n",
    "- Advanced color normalization for histopathologic images (see [MAG09], [ROY18])\n",
    "- More data augmentation (e.g. by adding some random noise)\n",
    "- More state of the art CNN arhcitectures, e.g. inception v4 (see [SZE17])\n",
    "- Use a higher zoom level (very time consuming)\n",
    "- Try to implement Scannet (see [LIN18]) \n",
    "    * Hundreds of faster prediction time\n",
    "    * For Prediction phase later it uses bigger tiles as input, e.g. 2868x2868 and outputs scores on a map with 86x86 pixels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"LIN18\"></a>[LIN18]\n",
    "        </td>\n",
    "        <td>\n",
    "Lin, H., Chen, H., Dou, Q., Wang, L., Qin, J., Heng, P.: Scannet: A fast and densescanning framework for metastastic breast cancer detection from whole-slide image. In: 2018 IEEE Winter Conference on Applications of Computer Vision (WACV).pp. 539–546 (March 2018). https://doi.org/10.1109/WACV.2018.00065\n",
    "        </td>\n",
    "    </tr>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"MAG09\"></a>[MAG09]\n",
    "        </td>\n",
    "        <td>\n",
    "Magee, D., Treanor, D., Crellin, D., Shires, M., Smith, K., Mohee, K., Quirke, P.:Colour normalisation in digital histopathology images. Proc Optical Tissue Imageanalysis in Microscopy, Histopathology and Endoscopy (MICCAI Workshop) (01 2009)\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"ROY18\"></a>[ROY18]\n",
    "        </td>\n",
    "        <td>\n",
    "Roy,   S.,   kumar   Jain,   A.,   Lal,   S.,   Kini,   J.:   A   study   about   color   nor-malizationmethodsforhistopathologyimages.Micron114,42–61(2018).https://doi.org/https://doi.org/10.1016/j.micron.2018.07.005,http://www.sciencedirect.com/science/article/pii/S0968432818300982\n",
    "        </td>\n",
    "    </tr>\n",
    "    <!--\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"KRI12\"></a>[KRI12]\n",
    "        </td>\n",
    "        <td>\n",
    "            Krizhevsky, A., Sutskever, I., & Hinton, G. E. (2012). Imagenet classification with deep convolutional neural networks. In Advances in neural information processing systems (pp. 1097-1105).\n",
    "        </td>\n",
    "    </tr>\n",
    "    -->\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"SZE17\"></a>[SZE17]\n",
    "        </td>\n",
    "        <td>\n",
    "            Szegedy, Christian, et al. \"Inception-v4, inception-resnet and the impact of residual connections on learning.\" AAAI. Vol. 4. 2017.\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"ZEI14\"></a>[ZEI14]\n",
    "        </td>\n",
    "        <td>\n",
    "            Zeiler, M. D., & Fergus, R. (2014, September). Visualizing and understanding convolutional networks. In European conference on computer vision (pp. 818-833). Springer, Cham.\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "exercise-train-cnn-tensorflow<br/>\n",
    "by Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
