{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# HTW Berlin - Angewandte Informatik - Advanced Topics - Exercise - Multiclass Logistic Regression (Softmax) with TensorFlow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Modules](#Python-Modules)\n",
    "* [Exercise - Multiclass Logistic Regression (Softmax) with tensorflow](#Exercise---Multiclass-Logistic-Regression-(Softmax)-with-tensorflow)\n",
    "  * [Training Data](#Trainin-Data)\n",
    "  * [Implement the Model](#Implement-the-Model)\n",
    "    * [Softmax](#Softmax)\n",
    "    * [Cross Entropy](#Cross-Entropy)\n",
    "    * [Gradient Descent](#Cross-Entropy)\n",
    "  * [Plot](#Plot)\n",
    "    * [Cost (Loss) over Iterations](#Cost-(Loss)-over-Iterations)\n",
    "    * [Decision Boundary After Training](#Decision-Boundary-After-Training)\n",
    "    * [Decision Boundary Before Training](#Decision-Boundary-Before-Training)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "In this exercise notebook you will implement a multiclass logistic regression model using TensorFlow. To do so, one would normally use TensorFlow's predefined functions for the softmax prediction, the cross-entropy costs and an optimizer based on the gradient descent update algorithm. \n",
    "\n",
    "Here you will not use any of them, but implement them yourself only using basic TensorFlow functions like `tf.matmul`, `tf.transpose`, etc. An exception is the `tf.gradients` function, which returns the gradient of a function with respect to a variable / list of variables. This gradient can then be used to define the update algorithm.\n",
    "\n",
    "Besides consolidating your theoretical knowledge about gradient descent, knowing how to use the TensorFlow's autograd feature can be very useful when you want to do anything which can be calculated with a gradient but is not covered with the standard built-ins, e.g. define your own cost and update function.\n",
    "\n",
    "In order to detect errors in your own code, execute the notebook cells containing `assert` or `assert_almost_equal`. In this notebook, however, these cells will only detect a small portion of possible errors, e.g. your implemented function returning a wrong shape."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "To complete this exercise notebook, you should possess knowledge about the following topics.\n",
    "* Logistic regression\n",
    "* Softmax function\n",
    "* Cross-entropy\n",
    "* Gradient descent\n",
    "* Basic TensorFlow dataflow (see below)\n",
    "\n",
    "The following material can help you to acquire this knowledge:\n",
    "* Softmax, cross-entropy, gradient descent:\n",
    " * Chapter 5 and 6 of the [Deep Learning Book](http://www.deeplearningbook.org/)\n",
    " * Chapter 5 of the book Pattern Recognition and Machine Learning by Christopher M. Bishop [BIS07]\n",
    "* Logistic Regression (binary):\n",
    " * Video 15.3 and following in the playlist [Machine Learning](https://www.youtube.com/watch?v=-Z2a_mzl9LM&list=PLD0F06AA0D2E8FFBA&t=740s&index=110)\n",
    "* TensorFlow:\n",
    " * [TensorFlow dataflow](https://www.tensorflow.org/guide/graphs)\n",
    " * [TensorFlow gradient computation](https://www.tensorflow.org/api_docs/python/tf/gradients)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "from numpy.testing import assert_almost_equal\n",
    "\n",
    "if int(tf.__version__.split('.')[0]) != 2:\n",
    "    raise Warning('ATTENTION: This notebook was designed with tensorflow version 1.xx in mind.\\n\\n Suggested tensorflow methods during the exercises might NOT function as described. We suggest installing the latest 1.xx version of tensorflow in a seperate environment to solve this exercise.')\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "tf.reset_default_graph()\n",
    "sess = tf.InteractiveSession()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Exercise - Multiclass Logistic Regression (Softmax) with TensorFlow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Training Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Given $m$ examples in our training data $\\mathcal D = \\{(\\vec x^{(1)}, y^{(1)}),(\\vec x^{(2)},y^{(2)}), \\dots (\\vec x^{(m)},y^{(m)})\\}$, with $\\vec x^{(1)}$ denoting the first feature vector and $y^{(1)}$ the corresponding class.\n",
    "\n",
    "We will create our own training data by drawing samples from different gaussian distributions, which our model should be capable of generalizing. To make things concrete we will be using:\n",
    "\n",
    "\n",
    " - two features $\\vec x = (x_1, x_2)^T$\n",
    " - three classes: $y \\in \\{ 0, 1, 2\\}$\n",
    " - 100 examples for each class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# class 0:\n",
    "# covariance matrix and mean\n",
    "cov0 = np.array([[5,-4],[-4,4]])\n",
    "mean0 = np.array([2.,3])\n",
    "# number of data points\n",
    "m0 = 100\n",
    "\n",
    "# class 1\n",
    "# covariance matrix and mean\n",
    "cov1 = np.array([[5,-3],[-3,3]])\n",
    "mean1 = np.array([0.5,0.5])\n",
    "m1 = 100\n",
    "\n",
    "# class 2\n",
    "# covariance matrix mean\n",
    "cov2 = np.array([[2,0],[0,2]])\n",
    "mean2 = np.array([8.,-5])\n",
    "m2 = 100\n",
    "\n",
    "# generate m0 gaussian distributed data points with\n",
    "# mean0 and cov0.\n",
    "r0 = np.random.multivariate_normal(mean0, cov0, m0)\n",
    "r1 = np.random.multivariate_normal(mean1, cov1, m1)\n",
    "r2 = np.random.multivariate_normal(mean2, cov2, m2)\n",
    "\n",
    "def plot_data(r0, r1, r2):\n",
    "    plt.figure(figsize=(7.,7.))\n",
    "    plt.scatter(r0[...,0], r0[...,1], c='r', marker='o', label=\"Klasse 0\")\n",
    "    plt.scatter(r1[...,0], r1[...,1], c='y', marker='o', label=\"Klasse 1\")\n",
    "    plt.scatter(r2[...,0], r2[...,1], c='b', marker='o', label=\"Klasse 2\")\n",
    "    plt.xlabel(\"$x_0$\")\n",
    "    plt.ylabel(\"$x_1$\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Let's visualize our training data\n",
    "\n",
    "plot_data(r0, r1, r2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "X = np.concatenate((r0, r1, r2), axis=0)\n",
    "X.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "y = np.concatenate((np.zeros(m0), np.ones(m1), 2 * np.ones(m2)))\n",
    "y.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# shuffle the data\n",
    "assert X.shape[0] == y.shape[0]\n",
    "perm = np.random.permutation(np.arange(X.shape[0]))\n",
    "#print(perm)\n",
    "X = X[perm]\n",
    "y = y[perm]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Implement the Model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Since we have concrete classes and not contiunous values, we have to implement logistic regression (opposed to linear regression). logistic regression implies the use of the logistic function. But as the number of classes exceeds two, we have to use the generalized form, the softmax function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "Implement softmax regression. This can be split into three subtasks:\n",
    " 1. Implement the softmax function for prediction.\n",
    " 2. Implement the computation of the cross-entropy loss.\n",
    " 3. Implement vanilla gradient descent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Softmax\n",
    "\n",
    "**Task 1:**\n",
    "\n",
    "Implement the softmax prediction $h_i$, defined for each class $i$ as:\n",
    "\n",
    "$$\n",
    "h_i = \\frac{\\exp(z_i)}{\\sum_{k=1}^c\\exp (z_k)}\n",
    "$$\n",
    "\n",
    "with $c$ denoting the class label and the net output $z_i$ for that class, where the whole vector $\\vec z$ is defined as:\n",
    "\n",
    "$$\n",
    "\\vec z = W \\vec{x} + \\vec b\n",
    "$$\n",
    "\n",
    "                   \n",
    "**Hint:**\n",
    "\n",
    "Remember that your functions should be able to handle multiple or even all $\\vec x$s.\n",
    "\n",
    "\n",
    "Evaluating softmax should look like:\n",
    "  \n",
    "        in> h.eval(feed_dict={x: X})\n",
    "    \n",
    "        out> array([[1.62411915e-08, 1.70372473e-03, 9.98296261e-01],\n",
    "                    [3.72431863e-08, 3.27572320e-03, 9.96724248e-01],\n",
    "                    [9.83378708e-01, 1.66097078e-02, 1.15793373e-05],\n",
    "                    ....."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### First we define Variables for the weigths W and bias b.\n",
    "### From Docstring:\n",
    "### \"A variable maintains state in the graph across calls to run() ...\n",
    "### ... constructor requires an initial value ...\" \n",
    "NUM_LABELS = 3\n",
    "NUM_FEATURES = 2\n",
    "D_TYPE = tf.float32\n",
    "I_TYPE = tf.int32\n",
    "W = tf.Variable(tf.random_uniform([NUM_FEATURES, NUM_LABELS], dtype=D_TYPE))\n",
    "b = tf.Variable(tf.zeros([NUM_LABELS], dtype=D_TYPE))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### And placeholders for the training data.\n",
    "### From Docstring:\n",
    "### \"This tensor will produce an error if evaluated. Its value must\n",
    "### be fed using the `feed_dict` optional argument to `Session.run()`\n",
    "\n",
    "### Using None in the first dimension allows to feed a variable number\n",
    "x = tf.placeholder(shape=[None, NUM_FEATURES], dtype=D_TYPE, name=\"features\")\n",
    "t = tf.placeholder(shape=[None], dtype=I_TYPE, name=\"targets\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### Variables must be initialized by running an `init` Op after having\n",
    "### launched the graph.  We first have to add the `init` Op to the graph.\n",
    "\n",
    "init_op= tf.global_variables_initializer()\n",
    "sess.run(init_op)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### Implement this function\n",
    "\n",
    "def net_output(x, W, b):\n",
    "    \"\"\"\n",
    "    Calculates the net output z = W * x + b.\n",
    "    \n",
    "    :x: Predicitons.\n",
    "    :x type: 2D-Tensor of type float32 with \n",
    "            shape (n_examples, n_features).\n",
    "    :W: Weight matrix.\n",
    "    :W type: 2D-Tensor of type float32 with \n",
    "            shape (n_features, n_classes).\n",
    "    :b: Weight matrix.\n",
    "    :b type: D-Tensor of type float32 with \n",
    "            shape (n_classes).\n",
    "        \n",
    "    :returns: The net output\n",
    "    :r type: 2D-Tensor of type float32\n",
    "            with shape (n_examples, n_classes).\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### Implement this function\n",
    "\n",
    "def softmax(z):\n",
    "    \"\"\"\n",
    "    Returns the normalized predictions z.\n",
    "    \n",
    "    :z: Predicitons.\n",
    "    :z type: 2D-Tensor of type float32 with \n",
    "            shape (n_examples, n_classes).\n",
    "    \n",
    "    :returns: softmax prediction.\n",
    "    :r type: Tensor with same type and shape as z.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "z = net_output(x, W, b)\n",
    "h = softmax(z)\n",
    "\n",
    "some_predictions = h.eval(feed_dict={x: X[0:2]})\n",
    "print(some_predictions)\n",
    "\n",
    "assert_almost_equal(some_predictions[0].sum(), 1.0)\n",
    "assert_almost_equal(some_predictions[1].sum(), 1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Cross-Entropy\n",
    "\n",
    "**Task 2:**\n",
    "\n",
    "Implement the computation of the cross-entropy loss. Don't use any build-in function of TensorFlow for the cross-entropy.\n",
    "\n",
    "\n",
    "**Reminder:**\n",
    "\n",
    "\\begin{equation}\n",
    "\\begin{split}\n",
    "H(p, q) & = \\sum_{i=0}^c p_i(x) \\cdot \\log \\frac{1}{q_i(x)} \\\\\n",
    " & = -\\sum_{i=0}^c p_i(x) \\cdot \\log q_i(x) \\\\\n",
    "\\end{split}\n",
    "\\end{equation}\n",
    "\n",
    "with\n",
    "* the number of classes c\n",
    "* the correct class distribution $p(x)$\n",
    "* and the predictions of our net $q(x)$ (softmax output)\n",
    "\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "Return the cross-entropy average: \n",
    "$$J(W,b) = \\frac{1}{m} \\sum_{j=1}^m H\\left(p(\\vec x^{(j)}),q(\\vec x^{(j)})\\right)$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### Implement this function\n",
    "\n",
    "def cross_entropy(targets, predictions):\n",
    "    \"\"\"\n",
    "    Computes the cross-entropy average.\n",
    "    \n",
    "    :targets: True classes as scalars.\n",
    "    :targets type: tf.Tensor with the shape (n_classes).\n",
    "    :predictions: predictions as softmax output \n",
    "    :predicitons type: tf.Tensor with shape (n_examples, n_classes).\n",
    "    \n",
    "    :returns: cross-entropy average.\n",
    "    :r type: Tensor of type float32\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# t is the tensorflow placeholder for the targets (class labels)\n",
    "cost = cross_entropy(t, h)\n",
    "\n",
    "some_cost = cost.eval(feed_dict={x: X, t: y})\n",
    "print(some_cost)\n",
    "\n",
    "assert some_cost.dtype == np.float32"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Gradient Descent\n",
    "\n",
    "**Task 3:**\n",
    "\n",
    "Implement gradient descent and train the model:\n",
    "- Implement the gradient descent _update rule_. Don't use any TensorFlow build-in optimizer!\n",
    "  * Use `tf.gradients` for computing the gradient.\n",
    "  * `tf.assign` for updating.\n",
    "- Iteratively apply the _update rule_ to minimize the loss. \n",
    "- Train for 100 epochs\n",
    "- Use minibatches with size 50  \n",
    "- Keep track of the costs after each epoch\n",
    "- Decide about an appropriate learning rate\n",
    "\n",
    "\n",
    "**Reminder:**\n",
    "\n",
    "Equation for the _update rule_:\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "W' & = W - \\alpha \\cdot  \\frac{\\partial}{\\partial W} J(W, b)\\\\\\\\\n",
    "b' & = b - \\alpha \\cdot \\frac{\\partial}{\\partial b} J(W, b)\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "### Complete this cell\n",
    "\n",
    "nb_epochs = 100\n",
    "minibatch_size = 50\n",
    "learning_rate = 1337 ### Decide about an appropriate learning rate\n",
    "\n",
    "cost_per_epoch = []\n",
    "\n",
    "### Your code ...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Cost (Loss) over Iterations\n",
    "\n",
    "Plot of the cost progress vs. iterations.\n",
    "\n",
    "The output should look similar to the following:\n",
    "\n",
    "<img src=\"https://gitlab.com/deep.TEACHING/educational-materials/raw/dev/media/klaus/shrinking_costs.png\" alt=\"Internet connection needed\"></img>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.plot(range(len(cost_per_epoch)), cost_per_epoch)\n",
    "plt.xlabel('# of iterations')\n",
    "plt.ylabel('cost')\n",
    "plt.title('Learning Progress')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Decision Boundary After Training\n",
    "\n",
    "The following function plots the data with the decision boundaries after the training. The model should be trained well enough to seperate most (roughly ~95%) of the data correctly. Use the following code for plotting.\n",
    "\n",
    "The output should look similar to the following:\n",
    "\n",
    "<img src=\"https://gitlab.com/deep.TEACHING/educational-materials/raw/dev/media/klaus/linear_decision_boundary_3_classes.png\" alt=\"Internet connection needed\"></img>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def plot_decision_boundary(iteration=None, x_min=-10, x_max=14, y_min=-10, y_max=10):    \n",
    "    fig = plt.figure(figsize=(8,8))\n",
    "  \n",
    "    plt.xlim(x_min, x_max)\n",
    "    plt.ylim(y_min, y_max)\n",
    "\n",
    "    delta = 0.1\n",
    "    a = np.arange(x_min, x_max+delta, delta)\n",
    "    b = np.arange(y_min, y_max+delta, delta)\n",
    "    A, B = np.meshgrid(a, b)\n",
    "\n",
    "    x_ = np.dstack((A, B)).reshape(-1, 2)\n",
    "\n",
    "    out = h.eval(feed_dict={x: x_})\n",
    "\n",
    "    ns = list()\n",
    "    ns.append(3)\n",
    "    ns.extend(A.shape)\n",
    "    out = out.T.reshape(ns)\n",
    "\n",
    "    plt.pcolor(A, B, out[0], cmap=\"Blues\", alpha=0.2)\n",
    "    plt.pcolor(A, B, out[1], cmap=('Oranges'), alpha=0.2)\n",
    "    plt.pcolor(A, B, out[2], cmap=('Greens'), alpha=0.2)\n",
    "    # lets visualize the data:\n",
    "    plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.Spectral)\n",
    "\n",
    "    plt.title(\"Decision boundaries in data space.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plot_decision_boundary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Decision Boundary Before Training\n",
    "\n",
    "Now we reinitialize our model's variables to visualize how the decision boundaries might have been before the training. Since we initilize our weights with `tf.random_uniform` this will look different for every execution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "sess.run(init_op)\n",
    "plot_decision_boundary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"GOO16\"></a>[GOO16]\n",
    "        </td>\n",
    "        <td>\n",
    "            Goodfellow, Ian, et al. Deep learning. Vol. 1. Cambridge: MIT press, 2016.\n",
    "        </td>\n",
    "    </tr>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"BIS07\"></a>[BIS07]\n",
    "        </td>\n",
    "        <td>\n",
    "            Christopher M. Bishop, Pattern recognition and machine learning, 5th Edition. Springer 2007, ISBN 9780387310732.\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "HTW Berlin - Angewandte Informatik - Advanced Topics - Exercise - Multiclass Logistic Regression (Softmax) with tensorflow <br/>\n",
    "by Christian Herta, Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Christian Herta, Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
