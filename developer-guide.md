# Developer Guide

This is a style guide explains how create and edit deep.TEACHING notebooks in a concise way.


## General Guidelines

- Create small and topic focused notebooks. Before your teaching material gets to complex, think about how you can split it up.
- Describe the requirement to understand the lesson as accurately as possible.
- Offer exercises to practice
- Does your notebook require program code that is not directly related to the teaching material, such as extensiv data preprocessing, use the related Python Module: [Deep Teaching Commons](https://gitlab.com/deep.TEACHING/deep-teaching-commons) to provide it.


## Git Branches

First of all, please do not make changes in the `master` branch.

Create your pull request for the `dev` branch. As soon as it is merged into `dev`, the website will be rebuilt and deployed under [dev.deep-teaching.org](https://dev.deep-teaching.org) as part of the Continuous Integration (CI) pipeline. The main purpose of the `dev` branch is to identify breaking changes in the website build process or in the HTML rendering.

We will merge `dev` into `master` at some point, which will then trigger an update for [www.deep-teaching.org](https://www.deep-teaching.org/).

If you don't want unfinished notebooks to appear on [dev.deep-teaching.org](https://dev.deep-teaching.org) or [www.deep-teaching.org](https://www.deep-teaching.org/) you should consider creating a feature branch for the notebook. You can merge your feature branch into `dev` as soon as it's ready.


## Git Folder Structure

If you contribute to that repository, please follow this folder structure:

    .
    ├── media
    │   ├── new_developer       # Create a your own media folder and declare under which license your materials may be used.
    ├── notebooks               #
    │   ├── topic A             #
    │   │   ├── my_new_notebook # Create your teaching notebook under a appropriate topic or create a new one. Your notebook name should indicate the teaching content.
    │   │   └── ...
    │   ├── topic B
    │   └── ...
    └── ...


## Notebook Structure

All notebooks have the same structure, so that all deep.TEACHING materials have a similar design. To maintain consistency we offer a [blueprint](notebooks/uncategorized/blueprint.ipynb) notebook that should be used to create your own teaching materials. The reference implementatition [notebooks/sequence-learning/exercise-bi-gram-language-model.ipynb](notebooks/sequence-learning/exercise-bi-gram-language-model.ipynb) serves as an example.


## Coding Style

Be compliant with the [PEP8 - Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)


## Links

Links are required to comply with linking behavior in Jupyter Lab, which means that all links from file A to file B are relative to the path of file A.

Consider this directory structure:

```no-highlight
.
├── foo.md
├── notebooks
│   ├── bar.ipynb
├── media
│   ├── baz.png
```

The following links will work:

```
[link from bar.ipynb to foo.md](../foo.md)
[link from foo.md to bar.ipynb](notebooks/bar.ipynb)
![embedding image in foo.md](media/baz.png)
```

It is possible to reference a section within a `*.md` markdown file.

```no-highlight
[Section Links](#Links)
```

An external link works as expected.

```no-highlight
[Educational Materials on Gitlab](https://gitlab.com/deep.TEACHING/educational-materials)
```

## Jupyter Lab

We are using Jupyter Lab, not Jupyter Notebook Server, as a development platform. Ensuring compatibility with Jupyter Lab is **first priority**. Making sure that the HTML version on [www.deep-teaching.org](https://www.deep-teaching.org/) renders correctly is considered a **sencond priority** and must be adressed without breaking Jupyter Lab compatibility. As of version 0.31.12, Jupyter Lab does not provide a way to use CSS styles in notebooks, therefore no custom CSS is used.


## Citation Style

Citation of literature is based on the [IEEE editorial style manual](https://www.ieee.org/conferences_events/conferences/publishing/style_references_manual.pdf). Only in the case of in-text citations we deviate from these specifications. Instead of a reference number we use the first three letters of the surname of the main author and the last two numbers of the publication year.
