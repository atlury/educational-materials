**THIS COURSE IS STILL IN PREPARATION**

## Reinforcement learning

This course aims to give a brief introduction into reinforment learning, its basic princibles and key terms. Alongside unsupervised and 
supervised learning, reinforcement learning is one of three fundamental ways of machine learning. In contrast to the other two, reinforcment
learning is based and learning through agent-envrionment interaction trying to maximize the reward getting from doing something right.
So in other words learning from experience. Reinforcement learning is needed in cases where the behavior of the enviroment we are acting in 
is partly or completly unknown. Areas of use are for examble autonomous driving or learning to play games (e.g. ATARI games).

This course covers the topics: Markov Desicsion Processes, Dymanic Programming, Monte Carlo Methods and Temporal Difference Learning, 
which should introduce the basic princibles and key terms of reinformcement learning and set the fundament for learning about more 
advanced topics. 

The course is based on the book [Reinforcement Learning: An Introduction](http://www.andrew.cmu.edu/course/10-703/textbook/BartoSutton.pdf) by 
Richard S. Sutton and Andrew G. Barto. This book is the book to learn about reinforcement learning and also freely available.


# List of Exercises

* [exercise-10-armed-bandits-testbed.ipynb](../notebooks/reinforcement-learning/exercise-10-armed-bandits-testbed.ipynb)
* [exercise-gridworld.ipynb](../notebooks/reinforcement-learning/gridworld.ipynb)
* [exercise-monte-carlo-frozenlake-gym.ipynb](../notebooks/reinforcement-learning/exercise-monte-carlo-frozenlake-gym.ipynb)
* [exercise-TD-q-learning-frozenlake-gym.ipynb](../notebooks/reinforcement-learning/exercise-TD-q-learning-frozenlake-gym.ipynb)
* [markov-decision-processes-and-optimal-control.ipynb](../notebooks/reinforcement-learning/markov-decision-processes-and-optimal-control.ipynb)
