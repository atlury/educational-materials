**THIS COURSE IS STILL IN PREPARATION**

## Scientific NumPy 

This course aims to give an overview over the basic tools provided by 
NumPy to work effciently with arrays, in any dimension, to lower your
computation time and save storage. 

For a full treatment of the topic or if you are looking for a specific use case we suggest looking on
[Numpy.org](https://numpy.org) and the provided [NumPy Quickstart](https://numpy.org/devdocs/user/quickstart.html) tutorial.
[Stack overflow](https://stackoverflow.com) is also a very usefull platform to get tips and tricks using NumPy, and Python in general, 
and always a good starting point to solve problems and get answers to specific questions.

To work effienctly with arrays is the basis of any fast machine learning
algorithm and therefore it is of great advantage to have same basic knowledge
about NumPy and working with arrays. 

The notebook [theory-numpy-and-arrays](../notebooks/scientific-python/theory-numpy-and-arrays.ipynb) summarizes a few usefull 
techniques, which can practiced in the exercise notebooks given in the list below.

# List of Exercises

* [exercise-python-numpy-basics](../notebooks/scientific-python/exercise-python-numpy-basics.ipynb)
* [exercise-indexing-and-slicing](../notebooks/scientific-python/exercise-indexing-and-slicing.ipynb)
* [exercise-shaping-and-merging](../notebooks/scientific-python/exercise-shaping-and-merging.ipynb)
* [exercise-fancy-indexing](../notebooks/scientific-python/exercise-fancy-indexing.ipynb)
* [exercise-broadcasting](../notebooks/scientific-python/exercise-broadcasting.ipynb)
